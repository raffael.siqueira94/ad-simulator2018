package com.estrela.Estrela;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * 
 * Classe que inicializa a framework spring boot.
 *
 */
@SpringBootApplication
public class EstrelaApplication {
	/**
	 * 
	 * Método de inicialização da aplicaçãoe exposição da porta 8080 no localhost para acesso ao sistema
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(EstrelaApplication.class, args);
	}
}