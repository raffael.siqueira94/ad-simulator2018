package com.estrela.Estrela.Model;

import estrela.modelos.Metricas;

public class PreencheTabelaVO {
	
	
	
	double mt1min;
	double mt1;
	double mt1max;
	double mt1p;

	double mw1min;
	double mw1;
	double mw1max;
	double mw1p;

	double mx1min;
	double mx1;
	double mx1max;
	double mx1p;

	double mn_q1min;
	double mn_q1;
	double mn_q1max;
	double mn_q1p;


	double mt2min;
	double mt2;
	double mt2max;
	double mt2p;

	double mw2min;
	double mw2;
	double mw2max;
	double mw2p;
	
	double mn_q2min;
	double mn_q2;
	double mn_q2max;
	double mn_q2p;

	double mdeltamin;
	double mdelta;
	double mdeltamax;
	double mdeltap;

	double vdelta_tabela_min;
	double vdelta_tabela;
	double vdelta_tabela_max;
	double vdelta_tabela_p;
	

	public PreencheTabelaVO(Metricas fila1, Metricas fila2) {
		mt1min = Metricas.obterIntervaloConfianca(fila1.getEsperanca_tempo_total_rodada()).x;
		mt1 = Metricas.obterEsperanca(fila1.getEsperanca_tempo_total_rodada());
		mt1max = Metricas.obterIntervaloConfianca(fila1.getEsperanca_tempo_total_rodada()).y;
		mt1p = Metricas.obterPrecisao(fila1.getEsperanca_tempo_total_rodada());
		
		mw1min = Metricas.obterIntervaloConfianca(fila1.getEsperanca_espera_rodada()).x;
		mw1 = Metricas.obterEsperanca(fila1.getEsperanca_espera_rodada());
		mw1max = Metricas.obterIntervaloConfianca(fila1.getEsperanca_espera_rodada()).y;
		mw1p = Metricas.obterPrecisao(fila1.getEsperanca_espera_rodada());
		
		mx1min = Metricas.obterIntervaloConfianca(fila1.getEsperanca_servico_rodada()).x;
		mx1 = Metricas.obterEsperanca(fila1.getEsperanca_servico_rodada());
		mx1max = Metricas.obterIntervaloConfianca(fila1.getEsperanca_servico_rodada()).y;
		mx1p = Metricas.obterPrecisao(fila1.getEsperanca_servico_rodada());
		
		mn_q1min = Metricas.obterIntervaloConfianca(fila1.getEsperanca_N_q_rodada()).x;
		mn_q1 = Metricas.obterEsperanca(fila1.getEsperanca_N_q_rodada());
		mn_q1max = Metricas.obterIntervaloConfianca(fila1.getEsperanca_N_q_rodada()).y;
		mn_q1p = Metricas.obterPrecisao(fila1.getEsperanca_N_q_rodada());
		
		mt2min  = Metricas.obterIntervaloConfianca(fila2.getEsperanca_tempo_total_rodada()).x;
		mt2 = Metricas.obterEsperanca(fila2.getEsperanca_tempo_total_rodada());
		mt2max = Metricas.obterIntervaloConfianca(fila2.getEsperanca_tempo_total_rodada()).y;
		mt2p = Metricas.obterPrecisao(fila2.getEsperanca_tempo_total_rodada());
		
		mw2min = Metricas.obterIntervaloConfianca(fila2.getEsperanca_espera_rodada()).x;
		mw2 = Metricas.obterEsperanca(fila2.getEsperanca_espera_rodada());
		mw2max = Metricas.obterIntervaloConfianca(fila2.getEsperanca_espera_rodada()).y;
		mw2p = Metricas.obterPrecisao(fila2.getEsperanca_espera_rodada());

		
		mn_q2min = Metricas.obterIntervaloConfianca(fila2.getEsperanca_N_q_rodada()).x;
		mn_q2 = Metricas.obterEsperanca(fila2.getEsperanca_N_q_rodada());
		mn_q2max = Metricas.obterIntervaloConfianca(fila2.getEsperanca_N_q_rodada()).y;
		mn_q2p = Metricas.obterPrecisao(fila2.getEsperanca_N_q_rodada());
		
		mdeltamin = Metricas.obterIntervaloConfianca(fila2.getEsperanca_delta_rodada()).x;
		mdelta = Metricas.obterEsperanca(fila2.getEsperanca_delta_rodada());
		mdeltamax = Metricas.obterIntervaloConfianca(fila2.getEsperanca_delta_rodada()).y;
		mdeltap = Metricas.obterPrecisao(fila2.getEsperanca_delta_rodada());
		
		vdelta_tabela_min =  Metricas.obterIntervaloConfianca(fila2.getVariancia_delta_rodada()).x;
		vdelta_tabela = Metricas.obterEsperanca(fila2.getVariancia_delta_rodada());
		vdelta_tabela_max =  Metricas.obterIntervaloConfianca(fila2.getVariancia_delta_rodada()).y;
		vdelta_tabela_p = Metricas.obterPrecisao(fila2.getVariancia_delta_rodada());
	}

	public double getMt1p() {
		return mt1p;
	}

	public void setMt1p(double mt1p) {
		this.mt1p = mt1p;
	}

	public double getMw1p() {
		return mw1p;
	}

	public double getMn_q1p() {
		return mn_q1p;
	}

	public void setMn_q1p(double mn_q1p) {
		this.mn_q1p = mn_q1p;
	}

	public double getMn_q2p() {
		return mn_q2p;
	}

	public void setMn_q2p(double mn_q2p) {
		this.mn_q2p = mn_q2p;
	}

	public void setMw1p(double mw1p) {
		this.mw1p = mw1p;
	}

	public double getMx1p() {
		return mx1p;
	}

	public void setMx1p(double mx1p) {
		this.mx1p = mx1p;
	}

	public double getMt2p() {
		return mt2p;
	}

	public void setMt2p(double mt2p) {
		this.mt2p = mt2p;
	}

	public double getMw2p() {
		return mw2p;
	}

	public void setMw2p(double mw2p) {
		this.mw2p = mw2p;
	}

	public double getMdeltap() {
		return mdeltap;
	}

	public void setMdeltap(double mdeltap) {
		this.mdeltap = mdeltap;
	}

	public double getVdelta_tabela_p() {
		return vdelta_tabela_p;
	}

	public void setVdelta_tabela_p(double vdelta_tabela_p) {
		this.vdelta_tabela_p = vdelta_tabela_p;
	}

	public double getMt1min() {
		return mt1min;
	}

	public void setMt1min(double mt1min) {
		this.mt1min = mt1min;
	}

	public double getMt1() {
		return mt1;
	}

	public void setMt1(double mt1) {
		this.mt1 = mt1;
	}

	public double getMt1max() {
		return mt1max;
	}

	public void setMt1max(double mt1max) {
		this.mt1max = mt1max;
	}

	public double getMw1min() {
		return mw1min;
	}

	public void setMw1min(double mw1min) {
		this.mw1min = mw1min;
	}

	public double getMw1() {
		return mw1;
	}

	public void setMw1(double mw1) {
		this.mw1 = mw1;
	}

	public double getMw1max() {
		return mw1max;
	}

	public void setMw1max(double mw1max) {
		this.mw1max = mw1max;
	}

	public double getMx1min() {
		return mx1min;
	}

	public void setMx1min(double mx1min) {
		this.mx1min = mx1min;
	}

	public double getMx1() {
		return mx1;
	}

	public void setMx1(double mx1) {
		this.mx1 = mx1;
	}

	public double getMx1max() {
		return mx1max;
	}

	public void setMx1max(double mx1max) {
		this.mx1max = mx1max;
	}

	public double getMn_q1min() {
		return mn_q1min;
	}

	public void setMn_q1min(double mn_q1min) {
		this.mn_q1min = mn_q1min;
	}

	public double getMn_q1() {
		return mn_q1;
	}

	public void setMn_q1(double mn_q1) {
		this.mn_q1 = mn_q1;
	}

	public double getMn_q1max() {
		return mn_q1max;
	}

	public void setMn_q1max(double mn_q1max) {
		this.mn_q1max = mn_q1max;
	}

	public double getMt2min() {
		return mt2min;
	}

	public void setMt2min(double mt2min) {
		this.mt2min = mt2min;
	}

	public double getMt2() {
		return mt2;
	}

	public void setMt2(double mt2) {
		this.mt2 = mt2;
	}

	public double getMt2max() {
		return mt2max;
	}

	public void setMt2max(double mt2max) {
		this.mt2max = mt2max;
	}

	public double getMw2min() {
		return mw2min;
	}

	public void setMw2min(double mw2min) {
		this.mw2min = mw2min;
	}

	public double getMw2() {
		return mw2;
	}

	public void setMw2(double mw2) {
		this.mw2 = mw2;
	}

	public double getMw2max() {
		return mw2max;
	}

	public void setMw2max(double mw2max) {
		this.mw2max = mw2max;
	}

	
	public double getMn_q2min() {
		return mn_q2min;
	}

	public void setMn_q2min(double mn_q2min) {
		this.mn_q2min = mn_q2min;
	}

	public double getMn_q2() {
		return mn_q2;
	}

	public void setMn_q2(double mn_q2) {
		this.mn_q2 = mn_q2;
	}

	public double getMn_q2max() {
		return mn_q2max;
	}

	public void setMn_q2max(double mn_q2max) {
		this.mn_q2max = mn_q2max;
	}

	public double getMdeltamin() {
		return mdeltamin;
	}

	public void setMdeltamin(double mdeltamin) {
		this.mdeltamin = mdeltamin;
	}

	public double getMdelta() {
		return mdelta;
	}

	public void setMdelta(double mdelta) {
		this.mdelta = mdelta;
	}

	public double getMdeltamax() {
		return mdeltamax;
	}

	public void setMdeltamax(double mdeltamax) {
		this.mdeltamax = mdeltamax;
	}

	public double getVdelta_tabela_min() {
		return vdelta_tabela_min;
	}

	public void setVdelta_tabela_min(double vdelta_tabela_min) {
		this.vdelta_tabela_min = vdelta_tabela_min;
	}

	public double getVdelta_tabela() {
		return vdelta_tabela;
	}

	public void setVdelta_tabela(double vdelta_tabela) {
		this.vdelta_tabela = vdelta_tabela;
	}

	public double getVdelta_tabela_max() {
		return vdelta_tabela_max;
	}

	public void setVdelta_tabela_max(double vdelta_tabela_max) {
		this.vdelta_tabela_max = vdelta_tabela_max;
	}
	
}
