package com.estrela.Estrela.Model;

import java.util.ArrayList;
import java.util.List;

import javax.swing.plaf.synth.SynthSeparatorUI;

import estrela.modelos.Metricas;

/**
 * 
 * Classe Virtual Object para que se gere o json passado ao script no navegador do cliente
 * com as listas contendo os valores do final de rodada para cada um do quesitos especificados no trabalho
 * 
 *
 */
public class MetricasVO {
	private List<Double> esperanca_delta_rodada;
	private List<Double> variancia_delta_rodada;
	private List<Double> esperanca_espera_rodada;
	private List<Double> esperanca_servico_rodada;
	private List<Double> esperanca_tempo_total_rodada;
	private List<Double> esperanca_N_q_rodada;
	private boolean simulacao_chegou_ao_fim;
	private int lastIndice;
	/**
	 * 
	 * @param firstIndice marco inicial para que se projete os arrays do simulador
	 * @param lastIndice marco final para que se projete os arrays do simulador
	 * @param simulacao_chegou_ao_fim variável para demarcar se ainda existem valores a serem pegos pelo js
	 * 			do cliente
	 * @param fila fila ao qual deseja-se pegar os valores para popular o array (pode ser a Fila1 ou a Fila2)
	 */
	public void populaDados(int firstIndice, int lastIndice, boolean simulacao_chegou_ao_fim, Metricas fila) {
		setSimulacao_chegou_ao_fim(simulacao_chegou_ao_fim);
		this.lastIndice = lastIndice;
		if (fila.getEsperanca_delta_rodada().size() > 1) {
			setEsperanca_delta_rodada(new ArrayList<>(fila.getEsperanca_delta_rodada().subList(firstIndice, lastIndice)));
			setVariancia_delta_rodada(new ArrayList<>(fila.getVariancia_delta_rodada().subList(firstIndice, lastIndice)));			
		}
		esperanca_espera_rodada = new ArrayList<>(fila.getEsperanca_espera_rodada().subList(firstIndice, lastIndice));
		esperanca_servico_rodada = new ArrayList<>(fila.getEsperanca_servico_rodada().subList(firstIndice, lastIndice));
		esperanca_tempo_total_rodada = new ArrayList<>(
				fila.getEsperanca_tempo_total_rodada().subList(firstIndice, lastIndice));
		esperanca_N_q_rodada = new ArrayList<>(fila.getEsperanca_N_q_rodada().subList(firstIndice, lastIndice));

	}
	/**
	 * 
	 * Getters and setters
	 * 
	 */

	public int getLastIndice() {
		return lastIndice;
	}

	public void setLastIndice(int lastIndice) {
		this.lastIndice = lastIndice;
	}

	public List<Double> getEsperanca_espera_rodada() {
		return esperanca_espera_rodada;
	}

	public void setEsperanca_espera_rodada(ArrayList<Double> esperanca_espera_rodada) {
		this.esperanca_espera_rodada = esperanca_espera_rodada;
	}

	public List<Double> getEsperanca_servico_rodada() {
		return esperanca_servico_rodada;
	}

	public void setEsperanca_servico_rodada(ArrayList<Double> esperanca_servico_rodada) {
		this.esperanca_servico_rodada = esperanca_servico_rodada;
	}

	public List<Double> getEsperanca_tempo_total_rodada() {
		return esperanca_tempo_total_rodada;
	}

	public void setEsperanca_tempo_total_rodada(ArrayList<Double> esperanca_tempo_total_rodada) {
		this.esperanca_tempo_total_rodada = esperanca_tempo_total_rodada;
	}

	public List<Double> getEsperanca_N_q_rodada() {
		return esperanca_N_q_rodada;
	}

	public void setEsperanca_N_q_rodada(ArrayList<Double> esperanca_N_q_rodada) {
		this.esperanca_N_q_rodada = esperanca_N_q_rodada;
	}

	public boolean isSimulacao_chegou_ao_fim() {
		return simulacao_chegou_ao_fim;
	}

	public void setSimulacao_chegou_ao_fim(boolean simulacao_chegou_ao_fim) {
		this.simulacao_chegou_ao_fim = simulacao_chegou_ao_fim;
	}

	public List<Double> getEsperanca_delta_rodada() {
		return esperanca_delta_rodada;
	}

	public void setEsperanca_delta_rodada(List<Double> esperanca_delta_rodada) {
		this.esperanca_delta_rodada = esperanca_delta_rodada;
	}

	public List<Double> getVariancia_delta_rodada() {
		return variancia_delta_rodada;
	}

	public void setVariancia_delta_rodada(List<Double> variancia_delta_rodada) {
		this.variancia_delta_rodada = variancia_delta_rodada;
	}

}
