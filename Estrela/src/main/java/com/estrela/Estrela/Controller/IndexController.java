package com.estrela.Estrela.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 
 * Classe para tratar a abertura de conexão com o servidor de simulacao
 *
 */
@Controller
public class IndexController {
	
	/**
	 * 
	 * Método que retorna a página index.html para o cliente comecar a interacao com o sistema de simulação
	 * @return
	 */
	@RequestMapping("/")
	public String index(Model model) {
		return "index";
	}
}
