package com.estrela.Estrela.Controller;

import java.util.ArrayList;

import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.estrela.Estrela.Model.MetricasVO;
import com.estrela.Estrela.Model.PreencheTabelaVO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import estrela.Simulador;
import estrela.SimuladorComInterrupcao;
import estrela.SimuladorSemInterrupcao;
import estrela.modelos.Metricas;

/**
 * 
 * Classe controller para gerir as requisições ao simulador
 *
 */
@Controller
public class SimularController {
	
	public Simulador simulador;
	
	/**
	 * 
	 * Método para se tratar um pedido de uma nova simulação ao servidor que vem do formulário do navegador.
	 * Para cada simulação cria-se uma thread que executa a mesma, dessa forma é possível deixar
	 * dinâmico o preenchimento dos gráficos no navegador do cliente
	 * 
	 * @param tamanho variavel com o tamanho de cada rodada
	 * @param utilizacao variavel com o valor da utilizacao (segundo o enunciado pode variar entre 0.1 e 0.7
	 * @param interrupcao variavel para marcar se deve utilizar o simulador com ou sem interrupcao
	 * @param request variavel com os valores da requisicao utilizada pelo framework spring boot
	 * @return .html resposável por rederizar os valores no navegador do cliente
	 */
	@RequestMapping("/simular")
	public String simular(@RequestParam(value = "tamanho", defaultValue = "1000") int tamanho,
			@RequestParam(value = "utilizacao") double utilizacao,
			@RequestParam(value = "interrupcao", defaultValue = "false") boolean interrupcao,
			HttpServletRequest request) {

		if (interrupcao) {
			simulador = new SimuladorComInterrupcao(utilizacao);
		} else {
			simulador = new SimuladorSemInterrupcao(utilizacao);
		}

		Thread th;
		if (request.getSession().getAttribute("thread") != null) {
			th = (Thread) request.getSession().getAttribute("thread");
			th.interrupt();
		}
		
		th = new Thread(new Runnable() {
			@Override
			public void run() {
				simulador.start(tamanho);
				System.out.println("Terminado");
			}
		});

		System.out.println(simulador.simuladorTipo());
		request.getSession().setAttribute("simulador", simulador);
		request.getSession().setAttribute("thread", th);
		request.setAttribute("tamanho", tamanho);
		request.setAttribute("utilizacao", utilizacao);
		request.setAttribute("interrupcao", interrupcao);

		th.start();
		return "simular";
	}

	/**
	 * 
	 * Método responsável por tratar os pedidos vindos do AJAX no navegador do cliente para se popular os gráficos.
	 * 
	 * @param firstIndex ultima posição representada nos gráficos do cliente.
	 * @param request variavel com os valores da requisicao utilizada pelo framework spring boot
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/getDados")
	public String getDados(@RequestParam(value = "ultimoIndiceRecebido") int firstIndex, HttpServletRequest request) {
		Simulador simulador = (Simulador) request.getSession().getAttribute("simulador");
		Metricas metricasFila1 = simulador.getMetricasFila1();
		Metricas metricasFila2 = simulador.getMetricasFila2();
		boolean simulacao_chegou_ao_fim = simulador.isSimulacao_chegou_ao_fim();

		int lastIndex = metricasFila2.getVariancia_delta_rodada().size();
		if ((lastIndex - firstIndex) > 100) {
			lastIndex = firstIndex + 100;
		}
		if (lastIndex < metricasFila2.getVariancia_delta_rodada().size())
			simulacao_chegou_ao_fim = false;
		return metricasEstrelaToJSON(firstIndex, lastIndex, simulacao_chegou_ao_fim, metricasFila1, metricasFila2);

	}

	/**
	 * 
	 * @param firstIndex inicio dos arrays com os valores a serem populados nos gráficos do cliente
	 * @param lastIndex fim dos arrays com os valores a serem populados nos gráficos do cliente
	 * @param simulacao_chegou_ao_fim marcador para se controlar a proxima requisicao vinda do cliente,
	 * 			neste caso caso ainda não tenha se colocado todos os valores nos gráficos uma nova
	 * 			requisição será gerada
	 * @param metricasFila1 variavel com os valores de métricas da fila1
	 * @param metricasFila2 variavel com os valores de métricas da fila2
	 * @return
	 */
	private String metricasEstrelaToJSON(int firstIndex, int lastIndex, boolean simulacao_chegou_ao_fim,
			Metricas metricasFila1, Metricas metricasFila2) {
		ArrayList<MetricasVO> metricasEstrela = new ArrayList<>();
		metricasEstrela.add(new MetricasVO());
		metricasEstrela.get(0).populaDados(firstIndex, lastIndex, simulacao_chegou_ao_fim, metricasFila1);
		metricasEstrela.add(new MetricasVO());
		metricasEstrela.get(1).populaDados(firstIndex, lastIndex, simulacao_chegou_ao_fim, metricasFila2);

		try {
			ObjectMapper objectMapper = new ObjectMapper();
			return objectMapper.writeValueAsString(metricasEstrela);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return "";
	}
	/**
	 * Funcao para atualizar a tabela ao final da simulacao
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/atualizaTabela")
	public String atualizaTabela (HttpServletRequest request) {
		
		Simulador simulador = (Simulador) request.getSession().getAttribute("simulador");
		PreencheTabelaVO ptVO = new PreencheTabelaVO (simulador.getMetricasFila1(), simulador.getMetricasFila2());
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			return objectMapper.writeValueAsString(ptVO);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}

}
