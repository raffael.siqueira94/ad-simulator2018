FROM openjdk:8u171-jdk-alpine3.7

COPY Estrela/target/estrela-1.0.0.jar /spring.jar

EXPOSE 8080

CMD java -jar /spring.jar
