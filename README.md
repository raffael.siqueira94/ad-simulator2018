# Simulador Estrela
O simulador estrela é um projeto desenvolvido para a disciplina de Avliação e Desempenho I do Departamento de Ciência da Computação da UFRJ.

Para usar o simulador basta dar clone no projeto e executar

```sh
$ docker-compose up -d
```

Após isso ele estará disponível na porta 80 do seu computador:

```sh
http://localhost/
```

Junto ao projeto está seu relatório enviado ao professor na entrega do mesmo.
