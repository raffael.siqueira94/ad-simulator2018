import java.util.Random;

import estrela.Simulador;
import estrela.SimuladorSemInterrupcao;
import estrela.modelos.Evento;
import estrela.modelos.EventoGeradorChegadasFila1;
import estrela.modelos.EventoGeradorChegadasFila2;

/**
 * 
 * Classe principal da aplicação, responsável por colher os dados inputados e
 * inicializar a simulação
 *
 */

public class Principal {

	public static void main(String[] args) {

		//testaSimulacaoApenasComFila1();
		//testaSimulacaoApenasComUmaFila2();
		testaQualidadeGeradorDeNumerosAleatorios ();
		//testaSimulacaoSemInterrupcaoTodaFuncionando();
	}

	private static void testaQualidadeGeradorDeNumerosAleatorios() {
		Random random = new Random (0);
		double acumulador = 0.0;
		for (int i = 0 ; i < 10; i++)
			acumulador += random.nextDouble();
		System.out.println(acumulador / 10.0);
		
	}

	@SuppressWarnings("unused")
	private static void testaSimulacaoSemInterrupcaoTodaFuncionando() {
		Simulador simulador = new SimuladorSemInterrupcao(0.7) {
			@Override
			public void start(int tam_rodadas) {
				setSimulacao_chegou_ao_fim(false);
				
				controlaRodada(tam_rodadas);

				setSimulacao_chegou_ao_fim(true);
				imprimirMetricas();
			}
		};
		simulador.start(4500);
		
	}

	/**
	 * Método que testa o simulador colocando apenas a fila de dados para que se obtenha as métricas dela
	 */
	@SuppressWarnings("unused")
	private static void testaSimulacaoApenasComFila1() {
		Simulador simulador = new SimuladorSemInterrupcao(0.7) {
			@Override
			public void instanciaGeradoresChegadas(double rho) {
				EventoGeradorChegadasFila1 geradorChegadasFila1 = new EventoGeradorChegadasFila1(rho);
				lista_de_eventos.add(geradorChegadasFila1);
			}

			@Override
			public void start(int tam_rodadas) {
				for (int i = 0; i < 10; i++) {
					int pacotes_restantes = tam_rodadas;
					Evento evento = null;
					do {
						// Próximo evento selecionado para tratação devida
						evento = lista_de_eventos.remove(0);
						// Verificação se este pacote é o primeiro da rodada para que possa ser guardado
						// o instante de tempo
						if (this.primeiroDaRodada) {
							this.primeiroDaRodada = false;
							this.inicioRodada = evento.getInstante_ocorrencia();
						}
						// Envio do evento para o método devido
						trataEvento(evento,false);

						// Se o evento tratado anteriormente for uma saída do servidor, significa que
						// avançamos um passo da rodada
						if (evento.getTipo_evento() == "SaidaServidor")
							pacotes_restantes--;
						// Se este foi o último pacote da rodada, armazena-se o instante de tempo desta
						// ocorrência
						if (pacotes_restantes == 0)
							fimRodada = evento.getInstante_ocorrencia();
					} while (pacotes_restantes > 0);
					metricasFila1.calculaAreaN_q(fila1.size(), evento.getInstante_ocorrencia());
					metricasFila1.fimRodada(getRodada_atual(), evento.getInstante_ocorrencia());
				}
			}
		};
		simulador.start(4500);
		simulador.metricasFila1.imprimirMetricasTotais();
	}

	/**
	 * Método que testa o simulador colocando apenas uma fila de voz para que se obtenha as métricas dela
	 */
	@SuppressWarnings("unused")
	private static void testaSimulacaoApenasComUmaFila2() {
		Simulador simulador = new SimuladorSemInterrupcao(0.7) {
			@Override
			public void instanciaGeradoresChegadas(double rho) {
				EventoGeradorChegadasFila2 geradorChegadasFila2 = new EventoGeradorChegadasFila2(0);
				lista_de_eventos.add(geradorChegadasFila2);
				ultima_ocorrencia_canal.put(0, 0.0);
			}

			@Override
			public void start(int tam_rodadas) {
				for (int i = 0; i < 10; i++) {
					int pacotes_restantes = tam_rodadas;
					Evento evento = null;
					do {
						// Próximo evento selecionado para tratação devida
						evento = lista_de_eventos.remove(0);
						// Verificação se este pacote é o primeiro da rodada para que possa ser guardado
						// o instante de tempo
						if (this.primeiroDaRodada) {
							this.primeiroDaRodada = false;
							this.inicioRodada = evento.getInstante_ocorrencia();
						}
						// Envio do evento para o método devido
						trataEvento(evento, false);

						// Se o evento tratado anteriormente for uma saída do servidor, significa que
						// avançamos um passo da rodada
						if (evento.getTipo_evento() == "SaidaServidor")
							pacotes_restantes--;
						// Se este foi o último pacote da rodada, armazena-se o instante de tempo desta
						// ocorrência
						if (pacotes_restantes == 0)
							fimRodada = evento.getInstante_ocorrencia();
					} while (pacotes_restantes > 0);
					metricasFila2.calculaAreaN_q(fila2.size(), evento.getInstante_ocorrencia());
					metricasFila2.fimRodada(getRodada_atual(), evento.getInstante_ocorrencia());
				}
			}
		};
		simulador.start(4500);
		simulador.metricasFila2.imprimirMetricasTotais();
	}
}
