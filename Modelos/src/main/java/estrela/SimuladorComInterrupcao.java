package estrela;

import estrela.modelos.Evento;
import estrela.modelos.EventoGeradorChegadasFila2;
import estrela.modelos.EventoSaidaServidor;
import estrela.modelos.Metricas;
import estrela.modelos.Pacote;

/**
 * 
 * Classe que provê a implementação específica para o Simulador Com Interrupção
 *
 */

public class SimuladorComInterrupcao extends Simulador {

	public SimuladorComInterrupcao(double rho) {
		super(rho);
	}
	
	/**
	 * Método que controla a fase transiente do simulador
	 * 
	 * @param interrupcao
	 */
	public void faseTransiente() {

		Evento evento = null;
		int tamanhoFaseTransiente = 0;

		if (rho < 0.2) {

		} else if (rho < 0.3) {

		} else if (rho < 0.4) {

		} else if (rho < 0.5) {

		} else if (rho < 0.6) {

		} else if (rho < 0.7) {

		} else {
			
		}

			do {
				evento = lista_de_eventos.remove(0);
				trataEvento(evento, true);
				if (evento.getTipo_evento() == "SaidaServidor")
					tamanhoFaseTransiente--;
			} while (tamanhoFaseTransiente == 0);

		// Descarta as métricas da fase transiente
		metricasFila1.setInicio_rodada(evento.getInstante_ocorrencia());
		metricasFila1.setUltima_alteracao_n_q(evento.getInstante_ocorrencia());

		metricasFila2.setInicio_rodada(evento.getInstante_ocorrencia());
		metricasFila2.setUltima_alteracao_n_q(evento.getInstante_ocorrencia());
	}

	/**
	 * Método responsável por tratar chegadas ao sistema de pacotes de voz
	 */
	public void trataEventoChegadaFila2(Evento evento, boolean isFaseTransiente) {
		// Conversão de um tipo Evento para um tipo EventoGeradorChegadasFila2
		EventoGeradorChegadasFila2 geradorChegadasFila2 = (EventoGeradorChegadasFila2) evento;
		// Geração do pacote que chegará ao sistema
		Pacote pacote_fila2 = geradorChegadasFila2.geraPacote(getRodada_atual());
		pacote_fila2.setInstante_chegada(evento.getInstante_ocorrencia());
		// Se o servidor estiver vazio, o pacote pode ir para o servidor
		// Se nesse ponto da implementação o servidor estiver vazio, quer dizer que a
		// fila também está, garantido pela
		// implementação do método trataEventoSaidaServidor
		if (servidor.getPacote() == null) {
			pacote_fila2.setInstante_entrada_servidor(evento.getInstante_ocorrencia());
			servidor.setPacote(pacote_fila2);
			servidor.setInstante_ocorrencia(geradorChegadasFila2.getInstante_ocorrencia());
			servidor.proximoInstanteOcorrencia();
			lista_de_eventos.add(servidor);
			// Se o servidor estiver ocupado por um pacote de Dados, interrompe-o
		} else {
			if (servidor.getPacote().getTipo_pacote() == "Dados") {
				// Soma a area do numero de pacotes o que estava até agora
				if (!isFaseTransiente)
					metricasFila1.calculaAreaN_q(fila1.size(), evento.getInstante_ocorrencia());
				Pacote pacote = servidor.getPacote();
				// Pega o pacote de dados do servidor, seta o instante em que foi interrompido,
				// e o
				// devolve para a fila. Colhe-se a métrica do estado da fila 1
				pacote.setInterrompido(true, evento.getInstante_ocorrencia());
				fila1.getLista_de_pacotes().add(0, pacote);

				removeEventoSaidaServidorAntigoAntesDaInterrupcao();

				// Coloca pacote de voz no servidor
				pacote_fila2.setInstante_entrada_servidor(evento.getInstante_ocorrencia());
				servidor.setPacote(pacote_fila2);
				servidor.setInstante_ocorrencia(geradorChegadasFila2.getInstante_ocorrencia());
				servidor.proximoInstanteOcorrencia();
				lista_de_eventos.add(servidor);
				// Se estiver ocupado por outro pacote de voz, vai para a fila
				// Colhe-se o estado atual da fila 2
			} else {
				// Soma a area do numero de pacotes o que estava até agora
				if (!isFaseTransiente)
					metricasFila2.calculaAreaN_q(fila2.size(), evento.getInstante_ocorrencia());
				fila2.add(pacote_fila2);
			}
		}

		geradorChegadasFila2.proximoInstanteOcorrencia();
		lista_de_eventos.add(geradorChegadasFila2);
	}
	

	@Override
	protected boolean confereCriterioParadaSimulador() {
		return conferePrecisaoDasMetricas(metricasFila2);
	}

	// Remove o evento interrompido da lista de eventos
	private void removeEventoSaidaServidorAntigoAntesDaInterrupcao() {
		for (int i = 0; i < lista_de_eventos.size(); i++) {
			Evento evento = lista_de_eventos.get(i);
			if (evento.getTipo_evento() == "SaidaServidor") {
				lista_de_eventos.remove(i);
				break;
			}
		}
	}

	/**
	 * Getters e setters da classe
	 */

	public Metricas getMetricasFila1() {
		return metricasFila1;
	}

	public void setMetricasFila1(Metricas metricasFila1) {
		this.metricasFila1 = metricasFila1;
	}

	public Metricas getMetricasFila2() {
		return metricasFila2;
	}

	public void setMetricasFila2(Metricas metricasFila2) {
		this.metricasFila2 = metricasFila2;
	}

	@Override
	public String simuladorTipo() {
		return "SimuladorComInterrupcao";
	}

	@Override
	protected boolean conferePacoteCriterioDeFimRodada(Evento evento) {
		return evento.getTipo_evento() == "SaidaServidor"
				&& ((EventoSaidaServidor) evento).getPacote().getRodada_pacote() == getRodada_atual();
	}
}
