package estrela;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import estrela.modelos.Evento;
import estrela.modelos.EventoGeradorChegadasFila1;
import estrela.modelos.EventoGeradorChegadasFila2;
import estrela.modelos.EventoSaidaServidor;
import estrela.modelos.FilaDeEspera;
import estrela.modelos.Metricas;
import estrela.modelos.Pacote;

/**
 * 
 * Classe responsável por gerenciar o simulador da aplicação e conter os métodos
 * que serão usados pelo simulador com interrupção e pelo sem interrupção
 * igualmente
 *
 */

public abstract class Simulador {
	public List<Evento> lista_de_eventos;
	public FilaDeEspera fila1;
	public FilaDeEspera fila2;
	public EventoSaidaServidor servidor;
	public Metricas metricasFila1;
	public Metricas metricasFila2;
	public Map<Integer, Double> ultima_ocorrencia_canal;
	private boolean simulacao_chegou_ao_fim;
	protected double inicioRodada;
	protected double fimRodada;
	protected boolean primeiroDaRodada;
	double rho;
	int eliminacoes = 0;
	private int rodada_atual;

	public abstract String simuladorTipo();

	// Método a ser implementado pela classe SimuladorComInterrupcao e
	// SimuladorSemInterrupcao
	public abstract void trataEventoChegadaFila2(Evento evento, boolean isFaseTransiente);
	

	/**
	 * Método que controla a fase transiente do simulador
	 * 
	 * @param interrupcao
	 */
	public abstract void faseTransiente() ;

	// Construtor para inicializar as variáveis necessárias
	public Simulador(double rho) {
		lista_de_eventos = new ArrayList<Evento>();
		ultima_ocorrencia_canal = new TreeMap<>();
		fila1 = new FilaDeEspera();
		fila2 = new FilaDeEspera();
		servidor = new EventoSaidaServidor();
		metricasFila1 = new Metricas(false);
		metricasFila2 = new Metricas(true);
		this.rho = rho;
		this.inicioRodada = 0.0;
		this.fimRodada = 0.0;
		this.primeiroDaRodada = true;

		rodada_atual = 0;

		instanciaGeradoresChegadas(rho);

		Collections.sort(lista_de_eventos);
	}

	public void instanciaGeradoresChegadas(double rho) {
		// Criação do gerador de chegadas de pacotes de dados
		EventoGeradorChegadasFila1 geradorChegadasFila1 = new EventoGeradorChegadasFila1(rho);
		lista_de_eventos.add(geradorChegadasFila1);

		// Criação dos 30 canais de voz
		for (int i = 0; i < 30; i++) {
			EventoGeradorChegadasFila2 geradorChegadasFila2 = new EventoGeradorChegadasFila2(i);
			lista_de_eventos.add(geradorChegadasFila2);
			ultima_ocorrencia_canal.put(i, 0.0);
		}
	}

	// Método responsável por iniciar a simulação
	public void start(int tam_rodadas) {
		setSimulacao_chegou_ao_fim(false);

		// faseTransiente();

		controlaRodada(tam_rodadas);

		setSimulacao_chegou_ao_fim(true);
		imprimirMetricas();
	}

	/**
	 * Método que controla as rodadas úteis do simulador
	 * 
	 * @param tam_rodadas taamanho inserido pelo usuário para o simulador
	 */
	protected void controlaRodada(int tam_rodadas) {
		// Loop que controla as rodadas
		do {
			int pacotes_restantes = tam_rodadas;
			Evento evento = null;
			do {
				// Próximo evento selecionado para tratação devida
				evento = lista_de_eventos.remove(0);
				// Verificação se este pacote é o primeiro da rodada para que possa ser guardado
				// o instante de tempo
				if (this.primeiroDaRodada) {
					this.primeiroDaRodada = false;
					this.inicioRodada = evento.getInstante_ocorrencia();
				}

				/**
				 * Se o evento tratado anteriormente for uma saída do servidor da fila mais
				 * afetada e a rodada do pacote for a atual, significa que avançamos um passo na
				 * simulação
				 */
				if (conferePacoteCriterioDeFimRodada(evento)) {
					pacotes_restantes--;
				}

				// Envio do evento para o método devido
				trataEvento(evento, false);

				// Se este foi o último pacote da rodada, armazena-se o instante de tempo desta
				// ocorrência
				if (pacotes_restantes == 0) {
					fimRodada = evento.getInstante_ocorrencia();
					rodada_atual++;
				}
			} while (pacotes_restantes > 0);

			// Acerta o estimador do tamanho da fila para que ela pegue os tamanhos até o
			// final da rodada
			metricasFila1.calculaAreaN_q(fila1.size(), evento.getInstante_ocorrencia());
			metricasFila2.calculaAreaN_q(fila2.size(), evento.getInstante_ocorrencia());

			// Finaliza-se a colheita de métricas e faz os cálculos necessários
			metricasFila1.fimRodada(rodada_atual, fimRodada - inicioRodada);
			metricasFila2.fimRodada(rodada_atual, fimRodada - inicioRodada);

			if (metricasFila2.getVariancia_delta_rodada().size() > 1) {
				// metricasFila2.imprimirMetricasTotais();
				if (confereCriterioParadaSimulador())
					break;
			}
		} while (true);
	}

	protected abstract boolean conferePacoteCriterioDeFimRodada(Evento evento);

	protected abstract boolean confereCriterioParadaSimulador();

	/**
	 * Metodo que confere todas as metricas para parar o simulador
	 * 
	 * @return boolean dizendo se já atingiu a precisao de confianca em todas as
	 *         metricas parar o simulador
	 */
	protected boolean conferePrecisaoDasMetricas(Metricas metricas) {
		boolean espera = conferePrecisao(metricas.getEsperanca_espera_rodada());
		boolean tempo_total = conferePrecisao(metricas.getEsperanca_tempo_total_rodada());
		boolean n_q = conferePrecisao(metricas.getEsperanca_N_q_rodada());
		if (metricas.isVoz()) {
			boolean esperanca_delta = conferePrecisao(metricas.getEsperanca_delta_rodada());
			boolean variancia_delta = conferePrecisao(metricas.getVariancia_delta_rodada());
			return n_q && tempo_total && espera && esperanca_delta && variancia_delta;
		} else {
			boolean servico = conferePrecisao(metricas.getEsperanca_servico_rodada());
			return n_q && servico && tempo_total && espera;
		}
	}

	/**
	 * Metodo que confere unitariamente uma metrica e retorna se ela ja atingiu o a
	 * precisao desejada
	 * 
	 * @param metricas qual das metricas sera avaliada pelo metodo para saber se
	 *                 unitariamente ela atingiu a precisao desejada
	 * @return
	 */
	private boolean conferePrecisao(List<Double> metricas) {
		if (Metricas.obterPrecisao(metricas) < .05)
			return true;
		else
			return false;
	}

	/**
	 * Método responsável apenas por imprimir no console as métricas encontradas
	 */
	public void imprimirMetricas() {
		System.out.println("Fila1: " + fila1.getLista_de_pacotes().size());
		System.out.println("Fila2: " + fila2.getLista_de_pacotes().size());
		System.out.println("----------------------");
		System.out.println("Métricas Fila 1:");
		metricasFila1.imprimirMetricasTotais();
		System.out.println("----------------------");
		System.out.println("Métricas Fila 2:");
		metricasFila2.imprimirMetricasTotais();
	}

	/**
	 * Primeira triagem que um evento passa antes de ser tratado, para que seja
	 * possível descobrir qual o tipo de evento está sendo tratado
	 * 
	 * @param evento
	 */
	protected void trataEvento(Evento evento, boolean isFaseTransiente) {
		switch (evento.getTipo_evento()) {
		case "GerarChegadaFila1":
			trataEventoChegadaFila1(evento, isFaseTransiente);
			break;
		case "GerarChegadaFila2":
			trataEventoChegadaFila2(evento, isFaseTransiente);
			break;
		case "SaidaServidor":
			trataEventoSaidaServidor(evento, isFaseTransiente);
			break;
		}
		Collections.sort(lista_de_eventos);
	}

	/**
	 * Método responsável por tratar o evento que seja do tipo SaídaServidor
	 * 
	 * @param evento
	 */
	private void trataEventoSaidaServidor(Evento evento, boolean isFaseTransiente) {
		// É passado pro pacote o instante de tempo que ele saiu do servidor
		((EventoSaidaServidor) evento).getPacote().setInstante_saida_servidor(evento.getInstante_ocorrencia());
		Pacote pacoteSainte = ((EventoSaidaServidor) evento).getPacote();
		pacoteSainte.setInstante_saida_servidor(evento.getInstante_ocorrencia());
		if (((EventoSaidaServidor) evento).getPacote().getTipo_pacote() == "Voz") {
			// Calcula-se o atraso entre pacotes de voz
			double delta;
			// Se for o primeiro pacote do período de atividade, o mesmo não terá seu delta
			// computado
			if (pacoteSainte.isPrimeiro()) {
				delta = 0.0;
			} else {
				delta = pacoteSainte.getInstante_saida_servidor()
						- (double) ultima_ocorrencia_canal.get(pacoteSainte.getCanal());
			}
			ultima_ocorrencia_canal.replace(pacoteSainte.getCanal(), pacoteSainte.getInstante_saida_servidor());

			pacoteSainte.setDelta(delta);

			// Enviamos o pacote para a classe métrica para que a mesma compute
			// os seus tempos
			if (pacoteSainte.getRodada_pacote() == rodada_atual && !isFaseTransiente)
				metricasFila2.levantaMetricasPacoteSaidaServidor(pacoteSainte);

		} else {
			// Enviamos o pacote para a classe métrica para que a mesma compute
			// os seus tempos
			if (pacoteSainte.getRodada_pacote() == rodada_atual && !isFaseTransiente)
				metricasFila1.levantaMetricasPacoteSaidaServidor(pacoteSainte);
		}
		// Após esvaziar o servidor, confere-se se existe algum pacote de voz esperando
		// na fila
		if (fila2.getLista_de_pacotes().size() > 0) {
			// calcula-se o tamanho da fila pois ela sera modificada
			if (!isFaseTransiente)
				metricasFila2.calculaAreaN_q(fila2.size(), evento.getInstante_ocorrencia());
			// Pega-se o primeiro pacote da fila e repassa o mesmo para o servidor
			Pacote pacote_servidor = fila2.getLista_de_pacotes().remove(0);
			pacote_servidor.setInstante_entrada_servidor(evento.getInstante_ocorrencia());
			servidor.setPacote(pacote_servidor);
			servidor.proximoInstanteOcorrencia();
			lista_de_eventos.add(servidor);
		} else {

			// Mesmo fluxo acima se repete para a fila 1, caso a fila 2 esteja vazia
			if (fila1.getLista_de_pacotes().size() > 0) {
				if (!isFaseTransiente)
					metricasFila1.calculaAreaN_q(fila1.size(), evento.getInstante_ocorrencia());
				Pacote pacote_servidor = fila1.getLista_de_pacotes().remove(0);
				pacote_servidor.setInstante_entrada_servidor(evento.getInstante_ocorrencia());
				if (pacote_servidor.isInterrompido()) {
					pacote_servidor.incluirTempoEsperaInterrupcao(evento.getInstante_ocorrencia());
				}
				servidor.setPacote(pacote_servidor);
				servidor.proximoInstanteOcorrencia();
				lista_de_eventos.add(servidor);

				// Se ambas as filas estiverem vazias, então nenhum evento é repassado ao
				// servidor
			} else {
				servidor.setPacote(null);
			}
		}
	}

	/**
	 * Método que tratará eventos de chegadas de pacotes de dados
	 * 
	 * @param evento
	 */
	private void trataEventoChegadaFila1(Evento evento, boolean isFaseTransiente) {
		// Conversão de um tipo Evento para um tipo EventoGeradorChegadasFila1
		EventoGeradorChegadasFila1 geradorChegadasFila1 = (EventoGeradorChegadasFila1) evento;
		// Geração do pacote que chegará ao sistema
		Pacote pacote_fila1 = geradorChegadasFila1.geraPacote(getRodada_atual());
		pacote_fila1.setInstante_chegada(evento.getInstante_ocorrencia());
		// Se o servidor estiver vazio, o pacote pode ir para o servidor
		// Se nesse ponto da implementação o servidor estiver vazio, quer dizer que a
		// fila também está, garantido pela
		// implementação do método trataEventoSaidaServidor
		if (servidor.getPacote() == null) {
			pacote_fila1.setInstante_entrada_servidor(evento.getInstante_ocorrencia());
			servidor.setPacote(pacote_fila1);
			servidor.setInstante_ocorrencia(evento.getInstante_ocorrencia());
			servidor.proximoInstanteOcorrencia();
			lista_de_eventos.add(servidor);
			// Se o servidor estiver ocupado, coloca o pacote no final da fila e armazena o
			// estado atual da fila
		} else {
			if (!isFaseTransiente)
				metricasFila1.calculaAreaN_q(fila1.size(), evento.getInstante_ocorrencia());
			fila1.add(pacote_fila1);
		}
		geradorChegadasFila1.proximoInstanteOcorrencia();
		lista_de_eventos.add(geradorChegadasFila1);
	}

	/**
	 * Getters and setters
	 */

	public Metricas getMetricasFila1() {
		return metricasFila1;
	}

	public void setMetricasFila1(Metricas metricasFila1) {
		this.metricasFila1 = metricasFila1;
	}

	public Metricas getMetricasFila2() {
		return metricasFila2;
	}

	public void setMetricasFila2(Metricas metricasFila2) {
		this.metricasFila2 = metricasFila2;
	}

	public boolean isSimulacao_chegou_ao_fim() {
		return simulacao_chegou_ao_fim;
	}

	public void setSimulacao_chegou_ao_fim(boolean simulacao_chegou_ao_fim) {
		this.simulacao_chegou_ao_fim = simulacao_chegou_ao_fim;
	}

	public int getRodada_atual() {
		return rodada_atual;
	}

	public void setRodada_atual(int rodada_atual) {
		this.rodada_atual = rodada_atual;
	}
}
