package estrela;

import estrela.modelos.Evento;
import estrela.modelos.EventoGeradorChegadasFila2;
import estrela.modelos.EventoSaidaServidor;
import estrela.modelos.Pacote;

/**
 * 
 * Classe que provê a implementação específica para o Simulador Com Interrupção
 *
 */

public class SimuladorSemInterrupcao extends Simulador {

	public SimuladorSemInterrupcao(double rho) {
		super(rho);
	}

	/**
	 * Método que controla a fase transiente do simulador
	 * 
	 * @param interrupcao
	 */
	public void faseTransiente() {

		Evento evento = null;
		int tamanhoFaseTransiente = 0;

		if (rho < 0.2) {

		} else if (rho < 0.3) {

		} else if (rho < 0.4) {

		} else if (rho < 0.5) {

		} else if (rho < 0.6) {

		} else if (rho < 0.7) {

		} else {

		}

		do {
			evento = lista_de_eventos.remove(0);
			trataEvento(evento, true);
			if (evento.getTipo_evento() == "SaidaServidor")
				tamanhoFaseTransiente--;
		} while (tamanhoFaseTransiente == 0);

		// Descarta as métricas da fase transiente
		metricasFila1.setInicio_rodada(evento.getInstante_ocorrencia());
		metricasFila1.setUltima_alteracao_n_q(evento.getInstante_ocorrencia());

		metricasFila2.setInicio_rodada(evento.getInstante_ocorrencia());
		metricasFila2.setUltima_alteracao_n_q(evento.getInstante_ocorrencia());
	}

	/**
	 * Método responsável por tratar chegadas ao sistema de pacotes de voz
	 */
	public void trataEventoChegadaFila2(Evento evento, boolean isFaseTransiente) {
		// Conversão de um tipo Evento para um tipo EventoGeradorChegadasFila2
		EventoGeradorChegadasFila2 geradorChegadasFila2 = (EventoGeradorChegadasFila2) evento;
		// Geração do pacote que chegará ao sistema
		Pacote pacote_fila2 = geradorChegadasFila2.geraPacote(getRodada_atual());
		pacote_fila2.setInstante_chegada(evento.getInstante_ocorrencia());
		// Se o servidor estiver vazio, o pacote pode ir para o servidor
		// Se nesse ponto da implementação o servidor estiver vazio, quer dizer que a
		// fila também está, garantido pela
		// implementação do método trataEventoSaidaServidor
		if (servidor.getPacote() == null) {
			pacote_fila2.setInstante_entrada_servidor(evento.getInstante_ocorrencia());
			servidor.setPacote(pacote_fila2);
			servidor.setInstante_ocorrencia(geradorChegadasFila2.getInstante_ocorrencia());
			servidor.proximoInstanteOcorrencia();
			lista_de_eventos.add(servidor);
			// Se estiver ocupado por outro pacote, vai para a fila
			// Colhe-se o estado atual da fila 2
		} else {
			if (!isFaseTransiente)
				metricasFila2.calculaAreaN_q(fila2.size(), evento.getInstante_ocorrencia());
			fila2.add(pacote_fila2);
		}
		geradorChegadasFila2.proximoInstanteOcorrencia();
		lista_de_eventos.add(geradorChegadasFila2);
	}

	@Override
	protected boolean confereCriterioParadaSimulador() {
		return conferePrecisaoDasMetricas(metricasFila1) && conferePrecisaoDasMetricas(metricasFila2);
	}

	@Override
	public String simuladorTipo() {
		return "SimuladorSemInterrupcao";
	}

	@Override
	protected boolean conferePacoteCriterioDeFimRodada(Evento evento) {
		return evento.getTipo_evento() == "SaidaServidor"
				&& ((EventoSaidaServidor) evento).getPacote().getTipo_pacote() == "Dados"
				&& ((EventoSaidaServidor) evento).getPacote().getRodada_pacote() == getRodada_atual();
	}
}
