package estrela.modelos;

import java.util.Random;

import org.apache.commons.math3.distribution.ExponentialDistribution;
import org.apache.commons.math3.distribution.UniformRealDistribution;

/**
 * Classe que modela os eventos de chegada da fila 1 (dados)
 */
public class EventoGeradorChegadasFila1 extends Evento implements EventoGeradorChegadas {

	private ExponentialDistribution distribuicaoChegada;
	private Random gerador_instante_proximo_evento;

	private Random gerador_tamanho_pacote;

	/**
	 * Construtor da classe
	 * @param rho Utilizacao da fila 1
	 * @param semente Semente para geracao de numeros aleatorios
	 */
	public EventoGeradorChegadasFila1(double rho) {
		super();
		double lambda = (250.0 * rho) / 755.0;
		setDistribuicaoChegada(new ExponentialDistribution(1.0 / lambda));
		setGerador_instante_proximo_evento(new Random(0));
		setGerador_tamanho_pacote(new Random(0));
		this.setInstante_ocorrencia(0);
		this.setTipo_evento("GerarChegadaFila1");
	}

	/**
	 * Implementacao de proximoInstanteOcorrencia que deve ser implementado em todas as classes que herdam de Evento
	 */
	@Override
	public void proximoInstanteOcorrencia() {
		atualizaInstanteOcorrenciaEvento(this.getInstante_ocorrencia());
	}

	/**
	 * Metodo que gera o pacote da fila 1 (dados)
	 */
	@Override
	public Pacote geraPacote(int rodada_atual) {
		Pacote pacote = new Pacote();
		pacote.setInstante_chegada(this.getInstante_ocorrencia());
		pacote.setTipo_pacote("Dados");
		pacote.setTamanho_pacote(Integer.toUnsignedLong(geraTamanhoPacote()));
		pacote.setRodada_pacote(rodada_atual);
		return pacote;
	}

	/**
	 * Metodo que gera o tamanho do pacote de dados segundo a pdf mista fornecida no enunciado do trabalho
	 * @return Retorna o tamanho do pacote de dados
	 */
	private int geraTamanhoPacote() {
		double amostra_uniforme = new UniformRealDistribution(0.0, 1.0).inverseCumulativeProbability(gerador_tamanho_pacote.nextDouble()); //uniforme entre 0 e 1
		double F1 = 0.3; //primeiro ponto de concentracao de probabilidade na pmf
		double F2 = F1 + (0.3 / 1436.0) * (512.0 - 64.0); //segundo ponto de concentracao de probabilidade
		double F3 = F2 + 0.1; //terceiro ponto de concentracao de probabilidade
		double F4 = F3 + (0.3 / 1436.0) * (1500.0 - 512.0); //quarto ponto de concentracao de probabilidade
		double b1 = F2 - 512.0 / 448.0 * (F2 - F1); //coeficiente linear da reta entre F2 e F1
		double b2 = F4 - 1500.0 / 988.0 * (F4 - F3); //coeficiente linear da reta entre F4 e F3

		if (amostra_uniforme >= 0 && amostra_uniforme < F1) {
			return 64;
		}

		else if (amostra_uniforme >= F1 && amostra_uniforme < F2) {
			return (int) Math.round((amostra_uniforme - b1) * 448.0 / (F2 - F1));
		}

		else if (amostra_uniforme >= F2 && amostra_uniforme < F3) {
			return 512;
		}

		else if (amostra_uniforme >= F3 && amostra_uniforme < F4) {
			return (int) Math.round((amostra_uniforme - b2) * 988.0 / (F4 - F3));
		}

		else {
			return 1500;
		}
	}

	/**
	 * Metodo que atualiza o tempo do evento para a proxima chegada (tempo exponencial)
	 * @param eventoAtual Instante do evento atual
	 */
	private void atualizaInstanteOcorrenciaEvento(double eventoAtual) {
		this.setInstante_ocorrencia(eventoAtual
				+ distribuicaoChegada.inverseCumulativeProbability(gerador_instante_proximo_evento.nextDouble()));
	}

	/**
	 * Getters e setters da classe EventoGeradorChegadasFila1
	 * 
	 */
	public Random getGerador_tamanho_pacote() {
		return gerador_tamanho_pacote;
	}

	public ExponentialDistribution getDistribuicaoChegada() {
		return distribuicaoChegada;
	}

	public void setDistribuicaoChegada(ExponentialDistribution distribuicaoChegada) {
		this.distribuicaoChegada = distribuicaoChegada;
	}

	public void setGerador_tamanho_pacote(Random gerador_tamanho_pacote) {
		this.gerador_tamanho_pacote = gerador_tamanho_pacote;
	}

	public Random getGerador_instante_proximo_evento() {
		return gerador_instante_proximo_evento;
	}

	public void setGerador_instante_proximo_evento(Random gerador_instante_proximo_evento) {
		this.gerador_instante_proximo_evento = gerador_instante_proximo_evento;
	}
}
