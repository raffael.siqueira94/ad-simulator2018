package estrela.modelos;

import java.util.Random;

import org.apache.commons.math3.distribution.ExponentialDistribution;
import org.apache.commons.math3.distribution.GeometricDistribution;

/**
 * Classe que modela os eventos de chegada da fila 2 (voz)
 */
public class EventoGeradorChegadasFila2 extends Evento implements EventoGeradorChegadas {

	private ExponentialDistribution exponentialDistribution;
	private GeometricDistribution geometricDistribution;
	private Random random_tempo_silencio;
	private Random random_numero_ocorrencias;
	private int numero_ocorrencias = 0;
	private double instante_ultima_ocorrencia;
	private int canal;
	private int contador_ocorrencias;

	/**
	 * Construtor da classe EventoChegadasFila2
	 * @param canal Canal do qual o pacote faz parte (1 a 30)
	 * @param semente_silencio Semente usada para gerar as amostras dos tempos de silencio
	 * @param semente_ocorrencias Semente usada para gerar as amostras do numero de pacotes de um periodo de atividade
	 */
	public EventoGeradorChegadasFila2(int canal) {

		super();

		this.setTipo_evento("GerarChegadaFila2");
		random_tempo_silencio = new Random();
		random_numero_ocorrencias = new Random();
		exponentialDistribution = new ExponentialDistribution(650.0);
		geometricDistribution = new GeometricDistribution(1.0 / 22.0);
		instante_ultima_ocorrencia = 0.0;
		setCanal(canal);

		this.setInstante_ocorrencia(
				exponentialDistribution.inverseCumulativeProbability(random_tempo_silencio.nextDouble()));

	}

	/**
	 * Implementacao de proximoInstanteOcorrencia que deve ser implementado em todas as classes que herdam de Evento
	 */
	@Override
	public void proximoInstanteOcorrencia() {
		if (contador_ocorrencias == 0) {
			numero_ocorrencias = geometricDistribution
					.inverseCumulativeProbability(random_numero_ocorrencias.nextDouble()) + 1;

			this.setInstante_ocorrencia(getInstante_ocorrencia() + 16.0
					+ exponentialDistribution.inverseCumulativeProbability(random_tempo_silencio.nextDouble()));
			contador_ocorrencias = numero_ocorrencias;
		} else {
			contador_ocorrencias --;
			this.setInstante_ocorrencia(this.getInstante_ocorrencia() + 16.0);
		}

	}

	/**
	 * Metodo que gera o pacote de voz
	 */
	@Override
	public Pacote geraPacote(int rodada_atual) {
		Pacote pacote = new Pacote();
		pacote.setTipo_pacote("Voz");
		pacote.setTamanho_pacote(64);
		pacote.setCanal(canal);
		pacote.setRodada_pacote(rodada_atual);
		
		if (contador_ocorrencias == numero_ocorrencias)
			pacote.setPrimeiro(true);
		else
			pacote.setPrimeiro(false);
		
		pacote.setInstante_chegada(this.getInstante_ocorrencia());

		return pacote;
	}

	/**
	 * Getters e setters da classe EventoGeradorChegadasFila2
	 */
	public int getContador_ocorrencias() {
		return contador_ocorrencias;
	}

	public void setContador_ocorrencias(int contador_ocorrencias) {
		this.contador_ocorrencias = contador_ocorrencias;
	}

	public ExponentialDistribution getExponentialDistribution() {
		return exponentialDistribution;
	}

	public void setExponentialDistribution(ExponentialDistribution exponentialDistribution) {
		this.exponentialDistribution = exponentialDistribution;
	}

	public GeometricDistribution getGeometricDistribution() {
		return geometricDistribution;
	}

	public void setGeometricDistribution(GeometricDistribution geometricDistribution) {
		this.geometricDistribution = geometricDistribution;
	}

	public Random getRandom_tempo_silencio() {
		return random_tempo_silencio;
	}

	public void setRandom_tempo_silencio(Random random_tempo_silencio) {
		this.random_tempo_silencio = random_tempo_silencio;
	}

	public Random getRandom_numero_ocorrencias() {
		return random_numero_ocorrencias;
	}

	public void setRandom_numero_ocorrencias(Random random_numero_ocorrencias) {
		this.random_numero_ocorrencias = random_numero_ocorrencias;
	}

	public int getNumero_ocorrencias() {
		return numero_ocorrencias;
	}

	public void setNumero_ocorrencias(int numero_ocorrencias) {
		this.numero_ocorrencias = numero_ocorrencias;
	}

	public double getInstante_ultima_ocorrencia() {
		return instante_ultima_ocorrencia;
	}

	public void setInstante_ultima_ocorrencia(double instante_ultima_ocorrencia) {
		this.instante_ultima_ocorrencia = instante_ultima_ocorrencia;
	}

	public int getCanal() {
		return canal;
	}

	public void setCanal(int canal) {
		this.canal = canal;
	}
}
