package estrela.modelos;



/**
 * Classe que modela os pacotes da simulacao
 *
 */
public class Pacote {

	private double instante_chegada; //instante em que o pacote chegou no sistema
	private double instante_entrada_servidor; //instante em que o pacote entrou em servico
	private double instante_saida_servidor; //instante em que o pacote terminou de ser transmitido
	private double tempo_no_servidor; //tempo decorrido dentro do servidor
	private double tempo_na_fila_de_espera; //tempo decorrido na fila de espera antes de ser servido
	private String tipo_pacote; //tipo de pacote (voz ou dados)
	private long tamanho_pacote; //tamanho do pacote em bytes
	private boolean interrompido; //se o pacote de dados ja foi ou n�o interrompido
	private int canal; //canal do qual o pacote de voz faz parte
	private boolean primeiro; //se o pacote � o primeiro de um periodo ativo
	private double delta; //diferenca de tempo entre a transmissao de um pacote e outro do mesmo periodo ativo (jitter)
	private double tempo_servico_interrupcao; //tempo total de servico de um pacote interrompido
	private double instante_ultima_interrupcao; //instante da ultima vez que um pacote de dados foi interrompido
	private int rodada_pacote; //rodada que o pacote pertence
	
	/**
	 * Construtor da classe Pacote
	 */
	public Pacote() {
		super();
		this.interrompido = false;
		this.tempo_servico_interrupcao = 0.0;
	}

	
	/**
	 * Getters e setter da classe Pacote
	 */
	
	public double getInstante_chegada() {
		return instante_chegada;
	}

	public void setInstante_chegada(double instante_chegada) {
		this.instante_chegada = instante_chegada;
	}

	public double getInstante_entrada_servidor() {
		return instante_entrada_servidor;
	}

	public void setInstante_entrada_servidor(double instante_entrada_servidor) {
		this.instante_entrada_servidor = instante_entrada_servidor;
	}

	public double getInstante_saida_servidor() {
		return instante_saida_servidor;
	}

	public void setInstante_saida_servidor(double instante_saida_servidor) {
		this.instante_saida_servidor = instante_saida_servidor;
	}

	public double getTempo_no_servidor() {
		return tempo_no_servidor;
	}

	public void setTempo_no_servidor(double tempo_no_servidor) {
		this.tempo_no_servidor = tempo_no_servidor;
	}

	public String getTipo_pacote() {
		return tipo_pacote;
	}

	public void setTipo_pacote(String tipo_pacote) {
		this.tipo_pacote = tipo_pacote;
	}

	public long getTamanho_pacote() {
		return tamanho_pacote;
	}

	public void setTamanho_pacote(long tamanho_pacote) {
		this.tamanho_pacote = tamanho_pacote;
	}

	public boolean isInterrompido() {
		return interrompido;
	}

	public double getTempo_na_fila_de_espera() {
		return tempo_na_fila_de_espera;
	}

	public void setTempo_na_fila_de_espera(double tempo_na_fila_de_espera) {
		this.tempo_na_fila_de_espera = tempo_na_fila_de_espera;
	}
	public int getCanal() {
		return canal;
	}

	public void setCanal(int canal) {
		this.canal = canal;
	}

	public boolean isPrimeiro() {
		return primeiro;
	}

	public void setPrimeiro(boolean primeiro) {
		this.primeiro = primeiro;
	}

	public double getDelta() {
		return delta;
	}

	public void setDelta(double delta) {
		this.delta = delta;
	}
	public double getTempo_servico_interrupcao() {
		return tempo_servico_interrupcao;
	}

	public void setTempo_servico_interrupcao(double tempo_servico_interrupcao) {
		this.tempo_servico_interrupcao = tempo_servico_interrupcao;
	}

	public void setInterrompido(boolean interrompido, double instante_interrupcao) {
		this.interrompido = interrompido;
		this.instante_ultima_interrupcao = instante_interrupcao;
	}

	public void incluirTempoServicoInterrupcao(double instante_interrupcao) {
		this.tempo_servico_interrupcao += (instante_interrupcao - this.instante_entrada_servidor);
	}

	public void incluirTempoEsperaInterrupcao(double instante_volta_servidor) {
		this.tempo_na_fila_de_espera += (instante_volta_servidor - this.instante_ultima_interrupcao);
	}


	public int getRodada_pacote() {
		return rodada_pacote;
	}


	public void setRodada_pacote(int rodada_pacote) {
		this.rodada_pacote = rodada_pacote;
	}

}
