package estrela.modelos;


/**
 * Classe simples que implementa uma tupla, utilizada para representar os intervalos de confianca, sendo X o limite inferior e Y o limite superior. 
 * @param <X> Tipo do limite inferior (no caso do simulador, sempre double)
 * @param <Y> Tipo do limite inferior (no caso do simulador, sempre double)
 */
public class Tupla<X, Y> {
	public final X x; 
	public final Y y; 
	
	
	public Tupla(X x, Y y) { 
		this.x = x; 
		this.y = y; 
	} 
}
