package estrela.modelos;
/*
 * Deve em todas as implementacoes extender a classe Evento,
 * pois nela está descrito o funcionamento do gerador como um Evento do simulador.
 */
public interface EventoGeradorChegadas {
	public Pacote geraPacote(int rodada_atual);
}
