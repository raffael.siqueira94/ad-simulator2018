package estrela.modelos;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * Classe que gerencia o tipo FilaDeEspera
 *
 */

public class FilaDeEspera {
	
	/**
	 * Construtor da classe
	 */
	public FilaDeEspera () {
		lista_de_pacotes = new ArrayList<Pacote>();
	}

	/**
	 * Lista dos pacotes que estarão na fila esperando serviços
	 */
	private List<Pacote> lista_de_pacotes;
	
	/**
	 * Getters e Setters
	 */
	public List<Pacote> getLista_de_pacotes() {
		return lista_de_pacotes;
	}

	public void setLista_de_pacotes(List<Pacote> lista_de_pacotes) {
		this.lista_de_pacotes = lista_de_pacotes;
	}

	/**
	 * Método para adicionar um pacote na fila
	 * @param pacote
	 */
	public void add(Pacote pacote) {
		lista_de_pacotes.add(pacote);
	}
	
	/**
	 * Método para retornar o tamanho da fila
	 * @param pacote
	 */
	public int size() {
		return lista_de_pacotes.size();
	}
}
