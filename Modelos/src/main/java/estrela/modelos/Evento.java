package estrela.modelos;
/**
 * Classe abstrata Evento que modela os possiveis eventos de uma simulacao
 */
public abstract class Evento implements Comparable<Evento> {
	/**
	 * Construtor da classe Evento
	 */
	public Evento() {
		super();
	}

	private double instante_ocorrencia; //instante de ocorrencia do evento
	private String tipo_evento; //tipo de evento (chegada da fila 1, chegada da fila 2 ou evento de saida do servidor)

	/**
	 * Metodo abstrato que cada implementacao de evento implementa de forma distinta, dependendo de sua logica
	 */
	public abstract void proximoInstanteOcorrencia();

	/**
	 * Outro construtor de Evento
	 * @param instante_ocorrencia Instante de ocorrencia do evento
	 */
	public Evento(double instante_ocorrencia) {
		super();
		this.instante_ocorrencia = instante_ocorrencia;
	}

	
	/**
	 * Getters e setters da classe Evento
	 */
	public double getInstante_ocorrencia() {
		return instante_ocorrencia;
	}

	public void setInstante_ocorrencia(double instante_ocorrencia) {
		this.instante_ocorrencia = instante_ocorrencia;
	}

	/**
	 * Metodo que compara dois eventos
	 */
	@Override
	public int compareTo(Evento evento2) {
		if (this.instante_ocorrencia < evento2.getInstante_ocorrencia()) {
			return -1;
		}
		if (this.instante_ocorrencia > evento2.getInstante_ocorrencia()) {
			return 1;
		}
		return 0;
	}
	
	public String getTipo_evento() {
		return tipo_evento;
	}

	public void setTipo_evento(String tipo_evento) {
		this.tipo_evento = tipo_evento;
	}

}
