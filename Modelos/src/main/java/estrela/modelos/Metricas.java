package estrela.modelos;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.distribution.TDistribution;

/**
 * 
 * Classe responsável por colher as métricas do simulador
 *
 */
public class Metricas {
	private double x;
	private double w;
	private double t;
	private boolean voz;
	private double area_total_n_q;
	private double ultima_alteracao_n_q;

	private double acumula_esperanca_espera;
	private double acumula_esperanca_servico;
	private double acumula_esperanca_tempo_total;
	private double acumula_esperanca_N_q;

	private ArrayList<Double> esperanca_delta_rodada;
	private ArrayList<Double> variancia_delta_rodada;
	private ArrayList<Double> esperanca_espera_rodada;
	private ArrayList<Double> esperanca_servico_rodada;
	private ArrayList<Double> esperanca_tempo_total_rodada;
	private ArrayList<Double> esperanca_N_q_rodada;

	private int qtd_pacotes;
	private double inicio_rodada;
	private double fim_rodada;
	private ArrayList<Double> acumulado_delta_amostral;

	/**
	 * Construtor da classe métricas, onde informamos se ela colherá metricas
	 * relacionadas a voz ou a dados
	 * 
	 * @param voz parâmetro para fazer a condição do tipo de pacotes a ser observado
	 */
	public Metricas(boolean voz) {
		this.x = 0.0;
		this.w = 0.0;
		this.t = 0.0;
		this.qtd_pacotes = 0;
		setInicio_rodada(0.0);
		this.voz = voz;
		area_total_n_q = 0.0;
		ultima_alteracao_n_q = 0.0;

		acumula_esperanca_espera = 0.0;
		acumula_esperanca_servico = 0.0;
		acumula_esperanca_tempo_total = 0.0;
		acumula_esperanca_N_q = 0.0;

		this.esperanca_delta_rodada = new ArrayList<Double>();
		this.variancia_delta_rodada = new ArrayList<Double>();
		this.esperanca_espera_rodada = new ArrayList<Double>();
		this.esperanca_servico_rodada = new ArrayList<Double>();
		this.esperanca_tempo_total_rodada = new ArrayList<Double>();
		this.esperanca_N_q_rodada = new ArrayList<Double>();
		this.acumulado_delta_amostral = new ArrayList<Double>();
	}

	/**
	 * Funcao que calcula o tamanho da fila
	 * 
	 * @param size                tamanho da fila de espera
	 * @param instante_ocorrencia instante da modificacao no tamanho da fila
	 */
	public void calculaAreaN_q(int size, double instante_ocorrencia) {
		area_total_n_q += (((double) size) * (instante_ocorrencia - ultima_alteracao_n_q));

		ultima_alteracao_n_q = instante_ocorrencia;

	}

	/**
	 * Método para calcular as métricas relativas a uma rodada e também zerar os
	 * acumuladores para a proxima rodada
	 * 
	 * @param duracaoRodada tempo de duração da rodada para levantar metricas
	 *                      relacionadas ao tempo na fila de um pacote qualquer
	 */
	public void fimRodada(int rodada_atual, double fim_rodada) {
		setFim_rodada(fim_rodada);

		double duracao_rodada = fim_rodada - inicio_rodada;
		acumula_esperanca_N_q += area_total_n_q / duracao_rodada;
		acumula_esperanca_espera += w / (double) qtd_pacotes;
		acumula_esperanca_servico += x / (double) qtd_pacotes;
		acumula_esperanca_tempo_total += t / (double) qtd_pacotes;

		esperanca_N_q_rodada.add(acumula_esperanca_N_q / (double) rodada_atual);
		esperanca_espera_rodada.add(acumula_esperanca_espera / (double) rodada_atual);
		
		esperanca_servico_rodada.add(acumula_esperanca_servico / (double) rodada_atual);
		esperanca_tempo_total_rodada.add(acumula_esperanca_tempo_total / (double) rodada_atual);

		if (voz) {
			esperanca_delta_rodada.add(obterEsperanca(acumulado_delta_amostral));
			variancia_delta_rodada.add(obterVariancia(acumulado_delta_amostral));
		}
		
		acumulado_delta_amostral.clear();
		this.x = 0.0;
		this.w = 0.0;
		this.t = 0.0;
		this.qtd_pacotes = 0;
		area_total_n_q = 0.0;
		ultima_alteracao_n_q = fim_rodada;
		setInicio_rodada(fim_rodada);
	}

	/**
	 * 
	 * @param medias array para calcular a media
	 * @return double com o valor da media
	 */
	public static double obterEsperanca(List<Double> medias) {
		double esperanca = 0.0;
		for (Double media : medias) {
			esperanca += media;
		}
		esperanca = esperanca / (double) medias.size();

		return esperanca;
	}

	/**
	 * 
	 * @param medias array com os valores das medias a serem obtidos o intervalo de
	 *               confiança dentre elas
	 * @return Tupla com o intervalo de confianca
	 */
	public static Tupla<Double, Double> obterIntervaloConfianca(List<Double> medias) {
		double inferior;
		double superior;
		TDistribution tstudent = new TDistribution((double) (medias.size() - 1));
		double t = tstudent.inverseCumulativeProbability(0.95);
		double estimador_variancia = obterVariancia(medias);
		inferior = obterEsperanca(medias) - (t * Math.sqrt(estimador_variancia) / Math.sqrt((double) medias.size()));
		superior = obterEsperanca(medias) + (t * Math.sqrt(estimador_variancia) / Math.sqrt((double) medias.size()));
		return new Tupla<Double, Double>(inferior, superior);
	}

	/**
	 * 
	 * @param metrica metrica para se encontrar a precisao do IC
	 * @return
	 */
	public static double obterPrecisao(List<Double> metrica) {
		TDistribution tstudent = new TDistribution((double) (metrica.size() - 1));
		double t = tstudent.inverseCumulativeProbability(0.95);
		double numerador = t * Math.sqrt(obterVariancia(metrica));
		double denominador = obterEsperanca(metrica) * Math.sqrt((double) metrica.size());
		return numerador / denominador;
	}

	/**
	 * 
	 * @param medias array com os valores das medias das rodadas
	 * @return variancia da metrica no simulador
	 */
	public static Double obterVariancia(List<Double> medias) {
		double somatorio_medias = 0.0;
		double diferenca = 0.0;
		for (Double rodada : medias) {
			somatorio_medias += rodada;
		}
		somatorio_medias /= (double) medias.size();
		for (int i = 0; i < medias.size(); i++) {
			diferenca += Math.pow(medias.get(i) - somatorio_medias, 2);
		}

		return diferenca / (double) (medias.size() - 1);
	}

	/**
	 * Método que imprime as métricas coletadas e guardadas nos arrays que são
	 * propriedades da classe.
	 */
	public void imprimirMetricasTotais() {
		System.out.println("E[X] = " + obterEsperanca(esperanca_servico_rodada));
		System.out.println("E[W] = " + obterEsperanca(esperanca_espera_rodada));
		System.out.println("E[T] = " + obterEsperanca(esperanca_tempo_total_rodada));
		System.out.println("E[Nq] = " + obterEsperanca(esperanca_N_q_rodada));
		if (voz) {
			System.out.println("E[delta] = " + obterEsperanca(esperanca_delta_rodada));

			System.out.println("V(delta) = " + obterEsperanca(variancia_delta_rodada));
		}

		Tupla<Double, Double> IC_T = obterIntervaloConfianca(esperanca_tempo_total_rodada);
		Tupla<Double, Double> IC_X = obterIntervaloConfianca(esperanca_servico_rodada);
		Tupla<Double, Double> IC_W = obterIntervaloConfianca(esperanca_espera_rodada);
		Tupla<Double, Double> IC_Nq = obterIntervaloConfianca(esperanca_N_q_rodada);

		System.out.println("Intervalo de confianca E[T]: [" + IC_T.x + ", " + IC_T.y + "]");
		System.out.println("Intervalo de confianca E[X]: [" + IC_X.x + ", " + IC_X.y + "]");
		System.out.println("Intervalo de confianca E[W]: [" + IC_W.x + ", " + IC_W.y + "]");
		System.out.println("Intervalo de confianca E[Nq]: [" + IC_Nq.x + ", " + IC_Nq.y + "]");

		if (voz) {
			Tupla<Double, Double> IC_Delta = obterIntervaloConfianca(esperanca_delta_rodada);
			Tupla<Double, Double> IC_VDelta = obterIntervaloConfianca(variancia_delta_rodada);
			System.out.println("Intervalo de confianca E[Delta]: [" + IC_Delta.x + ", " + IC_Delta.y + "]");
			System.out.println("Intervalo de confianca V(Delta): [" + IC_VDelta.x + ", " + IC_VDelta.y + "]");
		}

	}

	/**
	 * Contabiliza as métricas de um pacote
	 * 
	 * @param p pacote que será contabilizado
	 */
	public void levantaMetricasPacoteSaidaServidor(Pacote p) {
		qtd_pacotes++;
		if (p.isInterrompido()) {
			double ww = p.getTempo_na_fila_de_espera();
			double xx = p.getInstante_saida_servidor() - p.getInstante_entrada_servidor()
					+ p.getTempo_servico_interrupcao();
			this.w += ww;
			this.x += xx;
			this.t += ww + xx;
		} else {
			double ww = p.getInstante_entrada_servidor() - p.getInstante_chegada();
			double xx = p.getInstante_saida_servidor() - p.getInstante_entrada_servidor();
			double tt = ww + xx;
			this.w += ww;
			this.x += xx;
			this.t += tt;
		}
		if (p.getTipo_pacote() == "Voz") {
			if (p.getDelta() > 0.0)
				acumulado_delta_amostral.add(p.getDelta());
		}

	}

	/**
	 * 
	 * Getters and setters
	 */
	public ArrayList<Double> getEsperanca_N_q_rodada() {
		return esperanca_N_q_rodada;
	}

	public void setEsperanca_N_q_rodada(ArrayList<Double> esperanca_N_q_rodada) {
		this.esperanca_N_q_rodada = esperanca_N_q_rodada;
	}

	public boolean isVoz() {
		return voz;
	}

	public void setVoz(boolean voz) {
		this.voz = voz;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getW() {
		return w;
	}

	public void setW(double w) {
		this.w = w;
	}

	public double getT() {
		return t;
	}

	public void setT(double t) {
		this.t = t;
	}

	public ArrayList<Double> getEsperanca_espera_rodada() {
		return esperanca_espera_rodada;
	}

	public void setEsperanca_espera_rodada(ArrayList<Double> esperanca_espera_rodada) {
		this.esperanca_espera_rodada = esperanca_espera_rodada;
	}

	public ArrayList<Double> getEsperanca_servico_rodada() {
		return esperanca_servico_rodada;
	}

	public void setEsperanca_servico_rodada(ArrayList<Double> esperanca_servico_rodada) {
		this.esperanca_servico_rodada = esperanca_servico_rodada;
	}

	public ArrayList<Double> getEsperanca_tempo_total_rodada() {
		return esperanca_tempo_total_rodada;
	}

	public void setEsperanca_tempo_total_rodada(ArrayList<Double> esperanca_tempo_total_rodada) {
		this.esperanca_tempo_total_rodada = esperanca_tempo_total_rodada;
	}

	public ArrayList<Double> getEsperanca_delta_rodada() {
		return esperanca_delta_rodada;
	}

	public void setEsperanca_delta_rodada(ArrayList<Double> esperanca_delta_rodada) {
		this.esperanca_delta_rodada = esperanca_delta_rodada;
	}

	public ArrayList<Double> getVariancia_delta_rodada() {
		return variancia_delta_rodada;
	}

	public void setVariancia_delta_rodada(ArrayList<Double> variancia_delta_rodada) {
		this.variancia_delta_rodada = variancia_delta_rodada;
	}
	
	public double getInicio_rodada() {
		return inicio_rodada;
	}

	public void setInicio_rodada(double inicio_rodada) {
		this.inicio_rodada = inicio_rodada;
	}

	public double getFim_rodada() {
		return fim_rodada;
	}

	public void setFim_rodada(double fim_rodada) {
		this.fim_rodada = fim_rodada;
	}

	public double getArea_total_n_q() {
		return area_total_n_q;
	}

	public void setArea_total_n_q(double area_total_n_q) {
		this.area_total_n_q = area_total_n_q;
	}

	public double getUltima_alteracao_n_q() {
		return ultima_alteracao_n_q;
	}

	public void setUltima_alteracao_n_q(double ultima_alteracao_n_q) {
		this.ultima_alteracao_n_q = ultima_alteracao_n_q;
	}

}
