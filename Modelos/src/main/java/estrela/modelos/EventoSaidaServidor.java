package estrela.modelos;

/**
 * 
 * Classe que gerencia os tipos de evento chamados de Saída Servidor
 *
 */

public class EventoSaidaServidor extends Evento {
	private Pacote pacote;

	/**
	 * Construtor da classe
	 */
	public EventoSaidaServidor() {
		super();
		this.setTipo_evento("SaidaServidor");
	}

	/**
	 * Metódo que calcula o instante da próxima ocorrencia
	 * de saída do servidor
	 */
	@Override
	public void proximoInstanteOcorrencia() {	
		this.setInstante_ocorrencia(this.getInstante_ocorrencia()+
				(new Long(pacote.getTamanho_pacote())).doubleValue()/250.0);
		
		pacote.setInstante_saida_servidor(this.getInstante_ocorrencia());
	}
	
	/**
	 * Getter e setter
	 */

	public Pacote getPacote() {
		return pacote;
	}

	public void setPacote(Pacote pacote) {
		this.pacote = pacote;
	}

}
